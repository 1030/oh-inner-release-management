# OpenHarmony社区版本发布计划：

| **迭代计划**        | **版本**号                   | 版本目的 | **版本构建** | **版本转测试** | **版本测试完成** |
| ------------------- | ---------------------------- | ------------ | -------------- | ---------------- | ---------------- |
| MR1（API Level：8） | OpenHarmony 3.1.6.1       | 需求验收版本 | 2022/5/11    | 2022/5/12      | 2022/5/17        |
|                     | OpenHarmony 3.1.6.2 | Bugfix版本 | 2022/5/18    | 2022/5/19      | 2022/5/24        |
|                     | OpenHarmony 3.1.6.3 | 问题回归版本 | 2022/5/25    | 2022/5/25      | 2022/5/27        |
| OpenHarmony-v3.1.1-Release | OpenHarmony 3.1.6.5 | 发布版本 | 2022/5/27 | 2022/5/27  | 2022/5/30      |
|  | OpenHarmony 3.1.7.1 | Bugfix版本 | 2022/7/11 | 2022/7/11 | 2022/7/20 |
|  | OpenHarmony 3.1.7.2 | 问题回归版本 | 2022/7/20 | 2022/7/20 | 2022/7/25 |
| OpenHarmony-v3.1.2-Release | OpenHarmony 3.1.7.3 | 发布版本 | 2022/7/26 | 2022/7/26 | 2022/7/28 |

